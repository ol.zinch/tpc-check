
from datetime import datetime
from dictdiffer import diff
import csv
import json
import hashlib


def load_csv(filename):
    with open(filename) as csvfile:
        fd = csv.reader(csvfile, delimiter=',', strict=True)
        header = next(fd)
        map = {}
        for line in fd:
            row = dict(zip(header, line))
            row_json = json.dumps(row, sort_keys=True)
            key = hashlib.sha256(row_json.encode("utf8")).hexdigest()
            map[key] = row_json
        return map


def compare(expected_filename, actual_filename):
    t0 = datetime.now()
    expected = load_csv(expected_filename)
    t1 = datetime.now()
    print("expected:\n",expected)
    print("expected: load dt =",t1 - t0)
    actual = load_csv(actual_filename)
    t2 = datetime.now()
    print("actual:\n",actual)
    print("actual: load dt =",t2 - t1)

    result = {
        "added": [],
        "removed": []
    }
    for id in actual:
        if expected.get(id):
            del(expected[id])
        else:
            result["added"].append(actual[id])

    result["removed"] = list(expected.values())

    print("compare:dt =",datetime.now()-t2)
    return result


if __name__ == "__main__":

    cmp = compare("truth_71.csv", "check_71.csv")
    # cmp = compare("expected_71.csv", "actual_71.csv")
    print('\ndiff:\n  removed: {r}\n    added: {a}'.format(r=cmp.get('removed'), a=cmp.get('added')))